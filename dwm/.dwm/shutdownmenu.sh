#!/bin/sh

actions=("lock\nreboot\nshutdown\nsleep\nhibernate")

action=$(echo -e $actions | dmenu -fn "Mononoki:size=11")

case "$action" in
    lock)
        xscreensaver-command --lock
        ;;

    reboot)
        bash -c 'pkill -HUP X && sudo reboot'
        ;;

    shutdown)
        bash -c 'pkill -HUP X && sudo shutdown -h now'
        ;;

    sleep)
        bash -c 'pkill -HUP X; && sudo loginctl suspend'
        ;;

    hibernate)
        bash -c 'pkill -HUP X; && sudo loginctl hibernate'
        ;;
esac
